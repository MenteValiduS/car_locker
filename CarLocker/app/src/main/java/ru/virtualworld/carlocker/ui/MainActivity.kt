package ru.virtualworld.carlocker.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.provider.Settings
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.repositories.SettingsRepository
import ru.virtualworld.carlocker.services.OverlayService
import ru.virtualworld.carlocker.viewmodels.SettingsViewModel


class MainActivity : AppCompatActivity() {



    /*private lateinit var viewModel: SettingsViewModel

    companion object {
        private const val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        // Ask permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            val permissionIntent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(permissionIntent,
                CODE_DRAW_OVER_OTHER_APP_PERMISSION
            )
        }

        initViews()

        initViewModel()


    }

    private fun initViews() {
        btn_apply.setOnClickListener {
            saveSettingsInfo()
        }
    }


    private fun startOverlay() {
        startService(Intent(this@MainActivity, OverlayService::class.java))
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {

            if (resultCode == Activity.RESULT_OK) {
                // Вызывает overdraw layout
                startOverlay()
            } else {
                finish()
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    override fun onBackPressed() {
        startOverlay()

        launchTargetApp()
    }

    private fun launchTargetApp() {
        val settings = SettingsRepository.getTimingsSettings()
        val launchIntent = packageManager.getLaunchIntentForPackage(settings.packageName)

        if (launchIntent != null) {
            startActivity(launchIntent)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        viewModel.getSettingsData().observe(this, Observer { updateUI(it) })
    }

    private fun updateUI(timingsSettings: ru.virtualworld.carlocker.models.TimingsSettings) {
        et_timer_time.setText(timingsSettings.timeInMinutes.toString())
        et_package_name.setText(timingsSettings.packageName)
    }

    private fun  saveSettingsInfo() {
        ru.virtualworld.carlocker.models.TimingsSettings(
            et_timer_time.text.toString().toLong() * 60000,
            et_package_name.text.toString()
        ).apply {
            viewModel.saveSettingsData(this)
        }
    }*/

}