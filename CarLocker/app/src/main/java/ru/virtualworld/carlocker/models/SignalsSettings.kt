package ru.virtualworld.carlocker.models

data class SignalsSettings (
    val signalsArray: ArrayList<Signal>
) {
    fun getStringSignalArray(): ArrayList<String> {
        val array = arrayListOf<String>()

        signalsArray.forEach {
            array.add(it.toString())
        }

        return array
    }
}