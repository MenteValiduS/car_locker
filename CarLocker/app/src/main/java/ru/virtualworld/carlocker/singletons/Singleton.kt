package ru.virtualworld.carlocker.singletons

object Singleton {
    private var isSignalChanged = false

    fun signalChanged() {
        isSignalChanged = true
    }

    fun hasSignalChanged(): Boolean {

        if (isSignalChanged) {
            isSignalChanged = false
            return true
        } else return false

    }
}