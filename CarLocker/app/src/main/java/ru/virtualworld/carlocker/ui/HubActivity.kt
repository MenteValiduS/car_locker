package ru.virtualworld.carlocker.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_hub.*
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.services.OverlayService

class HubActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hub)

        initButtons()
    }

    override fun onBackPressed() {
        startOverlay()
    }

    private fun startOverlay() {
        startService(Intent(this@HubActivity, OverlayService::class.java))
        finish()
    }

    private fun initButtons() {

        btn_catch_signals_menu.setOnClickListener {
            val intent = Intent(this@HubActivity, CatchSignalActivity::class.java)
            startActivity(intent)
        }

        btn_set_signals_menu.setOnClickListener {
            val intent = Intent(this@HubActivity, SignalsSetActivity::class.java)
            startActivity(intent)
        }

        btn_settings_menu.setOnClickListener {
            val intent = Intent(this@HubActivity, SettingsActivity::class.java)
            startActivity(intent)
        }

        btn_timing_menu.setOnClickListener {
            val intent = Intent(this@HubActivity, TimingSettingsActivity::class.java)
            startActivity(intent)
        }

    }

}
