package ru.virtualworld.carlocker.ui

import android.os.Bundle
import android.os.Debug
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import kotlinx.android.synthetic.main.activity_signal_set.*
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.models.Signal
import ru.virtualworld.carlocker.models.SignalsSet
import ru.virtualworld.carlocker.repositories.SettingsRepository
import ru.virtualworld.carlocker.singletons.Singleton
import ru.virtualworld.carlocker.ui.Base.BaseSettingActivity
import java.lang.Exception

class SignalsSetActivity : BaseSettingActivity() {
    companion object {
        private const val COIN = "COIN"
        private const val START = "START"

    }

    private lateinit var signalsSet: SignalsSet
    private lateinit var signalsList: ArrayList<Signal>
    private var signalsStringList = mutableListOf<String>()
    private lateinit var signalsMap: MutableMap<String, Pair<Int, Int>>

    private var selectedCoinSignalID: Int = 0
    private var selectedStartSignalID: Int = 0

    //region Standard onMethods

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signal_set)

        initArrays()
        initAdapters()
        initUI()
    }

    //endregion

    //region Initializators

    private fun initArrays() {
        // Получение списка назначенных сигналов
        signalsSet = SettingsRepository.getSignalsSet()

        // Получение списка ВСЕХ сигналов
        val signalsSettings = SettingsRepository.getSignalsSettings()
        signalsList = signalsSettings.signalsArray
        signalsStringList = signalsSettings.getStringSignalArray()

        signalsMap = signalsSet.assignedSignalsMap.toMutableMap()
    }

    private fun initAdapters() {
        val coinSpinnerArrayAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item,
            signalsStringList
        )
        sp_signals_coin.adapter = coinSpinnerArrayAdapter

        val startSpinnerArrayAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item,
            signalsStringList
        )
        sp_signals_start.adapter = startSpinnerArrayAdapter

    }

    private fun initUI() {
        // Настройка позиций в компонентах Spinner
        with(sp_signals_coin) {

            try {
            val i = getSignalIndex(signalsList, signalsMap[COIN] ?: 0 to 0)
            this.setSelection(getSpinnerIndex(this, signalsList[i].toString()))
            } catch (e: Exception) {
            }

        }

        with(sp_signals_start) {
            try {

            val i = getSignalIndex(signalsList, signalsMap[START] ?: 0 to 0)
            this.setSelection(getSpinnerIndex(this, signalsList[i].toString()))
            } catch (e:Exception) {}
        }

        // Настройка событий при смене выбранного элемента в компонентах Spinner
        sp_signals_start.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedStartSignalID = position
            }

        }

        sp_signals_coin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedCoinSignalID = position
            }

        }


    }

    //endregion

    //region Buttons onClick Methods

    fun onAssignClick(view: View) {

        if (signalsMap.isNotEmpty()) {

            with(signalsList[selectedCoinSignalID]) {
                signalsMap[COIN] = this.signalCodeInt to this.vid
            }

            with(signalsList[selectedStartSignalID]) {
                signalsMap[START] = this.signalCodeInt to this.vid
            }

        } else {

            with(signalsList[selectedCoinSignalID]) {
                signalsMap.put(COIN, this.signalCodeInt to this.vid)
            }

            with(signalsList[selectedStartSignalID]) {
                signalsMap.put(START, this.signalCodeInt to this.vid)
            }

        }

        SettingsRepository.saveSignalsSet(SignalsSet(signalsMap))

        Singleton.signalChanged()

    }

    //endregion

    //region Private Methods

    /**
     * Получение позиции желаемого элемента
     */
    private fun getSpinnerIndex(spinner: Spinner, keyString: String): Int {

        for (i in 0..spinner.count) {

            if (spinner.getItemAtPosition(i) == keyString) {
                return i
            }

        }

        return 0
    }

    private fun getSignalIndex(signalsList: ArrayList<Signal>, pair: Pair<Int, Int>): Int {
        val (code, vid) = pair

        for (i in 0 until signalsList.size) {

            if (signalsList[i].signalCodeInt == code && signalsList[i].vid == vid) {
                return i
            }

        }

        return 0
    }

    //endregion
}
