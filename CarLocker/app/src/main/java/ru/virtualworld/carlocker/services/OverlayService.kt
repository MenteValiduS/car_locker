package ru.virtualworld.carlocker.services

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.PixelFormat
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbManager
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import androidx.core.view.isGone
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.repositories.SettingsRepository
import ru.virtualworld.carlocker.utils.Utils
import java.io.UnsupportedEncodingException
import android.os.Looper
import android.os.Handler
import android.widget.TextView
import ru.virtualworld.carlocker.singletons.Singleton


class OverlayService : Service() {
    companion object {
        private const val TICK = 1000L
        private const val WAIT_TIME_AFTER_CLICK = TICK * 10L
        private const val ARDUINO_VID = 6790
        private const val ACTION_USB_PERMISSION = "com.hariharan.arduinousb.USB_PERMISSION"
        private const val MESSAGE_TO_INJECT = "Вставьте купюру в купюроприёмник!"
        private const val COIN = "COIN"
        private const val START = "START"
    }

    //region Fields

    private var isClicked: Boolean = false
    private var isStarted: Boolean = false
    private var coinCounter: Int = 0
    private var status = Status.AWAITING

    private lateinit var mWindowManager: WindowManager
    private lateinit var mFloatingView: View
    private lateinit var mParams: WindowManager.LayoutParams
    private lateinit var mRootView: RelativeLayout
    private lateinit var mainMessageTextView: TextView

    //region Timers
    private lateinit var countDownTimerAfterClick: CountDownTimer
    private lateinit var countDownTimerToLaunchDemonstration: CountDownTimer
    private lateinit var countDownTimerToDemonstration: CountDownTimer
    private lateinit var countDownTimerAfterCoinInject: CountDownTimer
    private lateinit var countDownGameTimer: CountDownTimer
    private lateinit var countDownTimerForWarning: CountDownTimer
    private lateinit var countDownTimerForPaidWaiting: CountDownTimer
    private lateinit var countDownTimerForBlockAfterPaidWaiting: CountDownTimer
    private lateinit var countDownTimerAfterEnd: CountDownTimer
    private lateinit var checkTimer: CountDownTimer

    private lateinit var testTimerStart: CountDownTimer

    private lateinit var timerForCoin: CountDownTimer
    private lateinit var timerForStart: CountDownTimer
    //endregion

    //region UART Fields
    private lateinit var usbManager: UsbManager
    private var device: UsbDevice? = null
    var connection: UsbDeviceConnection? = null
    var serialPort: UsbSerialDevice? = null

    private lateinit var startSignalInfo: Pair<Int, Int>
    private lateinit var coinSignalInfo: Pair<Int, Int>

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == ACTION_USB_PERMISSION) {
                val granted = intent.extras!!.getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED)
                if (granted) {
                    connection = usbManager.openDevice(device)

                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection)
                    if (serialPort != null) {
                        if (serialPort!!.open()) { //Установить параметры последовательного соедниения.
                            serialPort!!.setBaudRate(9600)
                            serialPort!!.setDataBits(UsbSerialInterface.DATA_BITS_8)
                            serialPort!!.setStopBits(UsbSerialInterface.STOP_BITS_1)
                            serialPort!!.setParity(UsbSerialInterface.PARITY_NONE)
                            serialPort!!.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF)
                            serialPort!!.read(mCallback) //

                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN")
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL")
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED")
                }
            } else if (intent.action == UsbManager.ACTION_USB_DEVICE_ATTACHED) {
                startCommunication()
            } else if (intent.action == UsbManager.ACTION_USB_DEVICE_DETACHED) {
                stopCommunication()
            }
        }
    }

    var mCallback: UsbSerialInterface.UsbReadCallback = UsbSerialInterface.UsbReadCallback { arg0 ->
        // Определение метода обратного вызова, который вызывается при приеме данных.
        var data: String?
        try {
            data = String(arg0, charset("UTF-8"))

            when (data[data.length - 1]) {
                startSignalInfo.first.toChar() -> {
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {
                        mRootView.isGone = false
                        mainMessageTextView.text = "Сигнал 1 (монетка)"
                        timerForCoin.start()
                    }
                }
                coinSignalInfo.first.toChar() -> {
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {
                        mRootView.isGone = false
                        mainMessageTextView.text = "Сигнал 2 (старт)"
                        timerForStart.start()
                    }
                }
            }
            /*data = String(arg0, charset("UTF-8"))

            when (data[data.length - 1]) {
                startSignalInfo.first.toChar() -> {

                    if (coinCounter > 0 && !isStarted) {
                        isStarted = true
                        coinCounter--
                        countDownGameTimer.start()

                        if (!mRootView.isGone) {
                            mRootView.isGone = true
                        }

                        countDownTimerToLaunchDemonstration.cancel()
                        countDownTimerToDemonstration.cancel()
                        countDownTimerAfterCoinInject.cancel()
                        countDownTimerForWarning.cancel()
                        countDownTimerForPaidWaiting.cancel()
                        countDownTimerForBlockAfterPaidWaiting.cancel()
                        countDownTimerAfterEnd.cancel()
                    }

                }
                coinSignalInfo.first.toChar() -> {
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {

                        if (coinCounter == 0) {
                            status = Status.WARNING
                            mainMessageTextView.text = "Если игра не начнётся " +
                                    "через времени, то экран будет заблокирован!"
                            countDownTimerForWarning.start()
                        } else {
                            mainMessageTextView.text = MESSAGE_TO_INJECT
                            mRootView.isGone = true
                            countDownTimerForPaidWaiting.start()
                            status = Status.AWAITING
                        }

                        coinCounter++
                    }
                }
            }*/

        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }
    //endregion

    //endregion

    //region Standard onMethods

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.layout_overlay, null)
        usbManager = getSystemService(Context.USB_SERVICE) as UsbManager

        getSignalsCodes()
        initPosition()
        // Инициализация таймеров
        initTimers()
        initUI()


        val filter = IntentFilter()
        filter.addAction(ACTION_USB_PERMISSION)
        registerReceiver(broadcastReceiver, filter)

        startCommunication()
        initTestTimers()
        //initStartTimer()

        //countDownTimerToLaunchDemonstration.start()
    }

    private fun initTestTimers() {

        timerForCoin = object : CountDownTimer(10000L, TICK) {
            override fun onFinish() {
                mRootView.isGone = true
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

        timerForStart = object : CountDownTimer(10000L, TICK) {
            override fun onFinish() {
                mRootView.isGone = true
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

    }

    //endregion

    //region Initializators

    @SuppressLint("ClickableViewAccessibility")
    private fun initPosition() {

        mParams = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        mParams.gravity = Gravity.CENTER
        mParams.x = 0
        mParams.y = 0

        val (width, height) = Utils.getResolution(mWindowManager, this)
        mParams.width = width
        mParams.height = height

        mWindowManager.addView(mFloatingView, mParams)

        mRootView = mFloatingView.findViewById(R.id.ll_root)
        mainMessageTextView = mFloatingView.findViewById(R.id.tw_main_message)

    }

    /**
     * Метод, отвечающий за инициализацию всех таймеров.
     */
    private fun initTimers() {
        val settings = SettingsRepository.getTimingsSettings()

        // Таймер, срабатывающий после клика мышки.
        countDownTimerAfterClick = object : CountDownTimer(WAIT_TIME_AFTER_CLICK, TICK) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                mRootView.isGone = false
                isClicked = false
            }
        }

        // Таймер, считающий время до демонстрации.
        countDownTimerToLaunchDemonstration = object : CountDownTimer(settings.awaitingTime, TICK) {
            override fun onFinish() {
                status = Status.DEMONSTRATION
                mRootView.isGone = true
                countDownTimerToDemonstration.start()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

        // Таймер для демонстрации.
        countDownTimerToDemonstration = object : CountDownTimer(settings.demonstrationTime, TICK) {
            override fun onFinish() {
                status = Status.AWAITING
                mRootView.isGone = false
                countDownTimerToLaunchDemonstration.start()
            }

            override fun onTick(millisUntilFinished: Long) {
            }

        }

        // Таймер, срабатывающий при отображении предупреждения при внесении первой монетки.
        countDownTimerForWarning = object : CountDownTimer(settings.warningTime, TICK) {
            override fun onFinish() {
                status = Status.COIN
                countDownTimerAfterCoinInject.start()
            }

            override fun onTick(millisUntilFinished: Long) {
            }

        }

        // Таймер, срабатывающий после внесения монетки.
        countDownTimerAfterCoinInject = object : CountDownTimer(settings.afterCoinInjectTime, TICK) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                // Если не было нажата кнопка Start, то переходим в режим блокировки.
                mRootView.isGone = false
                // И запускаем таймер для демонстрации.
                status = Status.PAID_WAITING
                countDownTimerForPaidWaiting.start()
            }
        }

        // Таймер, срабатывающий при начале игры.
        countDownGameTimer = object : CountDownTimer(settings.gameTime, TICK) {
            override fun onFinish() {
                status = Status.END
                isStarted = false
                countDownTimerAfterEnd.start()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

        countDownTimerAfterEnd = object : CountDownTimer(settings.afterEndGameTime, TICK) {
            override fun onFinish() {
                status = Status.DEMONSTRATION
                mRootView.isGone  = false
                countDownTimerToLaunchDemonstration.start()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

        // Таймер, ожидающий установленное время после вноса денег.
        countDownTimerForPaidWaiting = object : CountDownTimer(TICK * 1L, TICK) {
            override fun onFinish() {
                // Мы блокируем экран, так как время ожидания после оплаты закончилось.
                status = Status.AWAITING
                // Выводим предупреждение об окончании этого времени.
                mainMessageTextView.text = "Время ожидания закончилось!\nВнесите купюру."
                // И запускаем таймер, по истечении которого будет возвращено обычное сообщение.
                countDownTimerForBlockAfterPaidWaiting.start()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

        // Таймер, отсчитывающий время для объяснения блокировки экрана.
        countDownTimerForBlockAfterPaidWaiting = object : CountDownTimer(TICK * 10L, TICK) {
            override fun onFinish() {
                // Возвращаем стандартное сообщение.
                mainMessageTextView.text = MESSAGE_TO_INJECT
                // Так как мы перешли в режим ожидания запускаем отсчёт до демонстрации экрана.
                countDownTimerToLaunchDemonstration.start()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initUI() {

        mRootView.setOnTouchListener { _, _ ->

            if (status != Status.COIN || status != Status.DEMONSTRATION || status != Status.GAME) {
                mRootView.isGone = true
                countDownTimerAfterClick.start()
                isClicked = true
                true
            } else {
                false
            }

        }

    }

    private fun initStartTimer() {
        checkTimer = object : CountDownTimer(TICK * 5L, TICK) {
            override fun onFinish() {

                if (Singleton.hasSignalChanged()) {
                    getSignalsCodes()
                }

                checkTimer.start()
            }

            override fun onTick(millisUntilFinished: Long) {
            }

        }

        testTimerStart = object : CountDownTimer(WAIT_TIME_AFTER_CLICK, TICK) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                mRootView.isGone = false
            }
        }
    }

    //endregion

    //region UART methods
    fun startCommunication() {
        val usbDevices = usbManager.deviceList

        if (!usbDevices.isEmpty()) {
            var keep = true

            for (entry in usbDevices.entries) {
                device = entry.value
                val deviceVID = device!!.vendorId

                if (deviceVID == startSignalInfo.second) {
                    val pi = PendingIntent.getBroadcast(
                        this, 0,
                        Intent(ACTION_USB_PERMISSION), 0
                    )
                    usbManager.requestPermission(device, pi)
                    keep = false
                } else {
                    connection = null
                    device = null
                }

                if (!keep) {
                    break
                }

            }

        }

    }

    fun stopCommunication() {
        serialPort?.close()
    }


    //endregion

    //region Private Methods

    private fun getSignalsCodes() {
        val s = SettingsRepository.getSignalsSet().assignedSignalsMap

        startSignalInfo = s[START] ?: error("")
        coinSignalInfo = s[COIN] ?: error("")
    }

    //endregion

    //region SubClasses

    enum class Status {
        AWAITING,
        COIN,
        WARNING,
        PAID_WAITING,
        DEMONSTRATION,
        GAME,
        END
    }

    //endregion

}
