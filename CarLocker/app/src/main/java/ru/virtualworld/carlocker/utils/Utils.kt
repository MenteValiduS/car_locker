package ru.virtualworld.carlocker.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

object Utils {
    fun convertDpToPixel(dp: Float, context: Context): Int {
        return Math.round(dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun convertPixelsToDp(px: Float, context: Context): Int {
        return Math.round(px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun convertSpToPixel(sp: Float, context: Context): Int {
        return Math.round(sp * context.resources.displayMetrics.scaledDensity)
    }

    fun getResolution(windowManager: WindowManager, context: Context): Pair<Int, Int> {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels to
                displayMetrics.heightPixels
    }
}