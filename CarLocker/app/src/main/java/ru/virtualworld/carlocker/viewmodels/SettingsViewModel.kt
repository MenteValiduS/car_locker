package ru.virtualworld.carlocker.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import ru.virtualworld.carlocker.models.TimingsSettings
import ru.virtualworld.carlocker.repositories.SettingsRepository

class SettingsViewModel : ViewModel() {
    private val repository: SettingsRepository = SettingsRepository
    private val settingsData = MediatorLiveData<TimingsSettings>()

    init {
        settingsData.value = repository.getTimingsSettings()
    }

    fun getSettingsData(): LiveData<TimingsSettings> = settingsData

    fun saveSettingsData(timingsSettings: TimingsSettings) {
        repository.saveTimingsSettings(timingsSettings)
        settingsData.value = timingsSettings
    }

}