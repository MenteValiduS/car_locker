package ru.virtualworld.carlocker.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ru.virtualworld.carlocker.services.OverlayService

class StartMyServiceAtBootReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.action)) {
            val serviceIntent = Intent(context, OverlayService::class.java)
            serviceIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startService(serviceIntent)
        }

    }
}