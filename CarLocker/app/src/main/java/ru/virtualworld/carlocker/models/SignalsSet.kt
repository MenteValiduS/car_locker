package ru.virtualworld.carlocker.models

data class SignalsSet (
    // Pair<Signal, VID>
    val assignedSignalsMap: Map<String, Pair<Int,Int>>
) {
    fun getSignalInfoByName(signalName: String): Pair<Int, Int> {
        return assignedSignalsMap[signalName] ?: -1 to -1
    }
}