package ru.virtualworld.carlocker.ui

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import kotlinx.android.synthetic.main.activity_catch_signal.*
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.models.Signal
import ru.virtualworld.carlocker.models.SignalsSettings
import ru.virtualworld.carlocker.repositories.SettingsRepository
import ru.virtualworld.carlocker.ui.Base.BaseSettingActivity
import java.io.UnsupportedEncodingException
import java.util.HashMap

class CatchSignalActivity : BaseSettingActivity() {
    companion object {
        private const val ACTION_USB_PERMISSION = "com.hariharan.arduinousb.USB_PERMISSION"
    }

    //region Fields

    //region UART

    private var currentVID: Int = 0
    private var usbManager: UsbManager? = null
    private var device: UsbDevice? = null
    private var connection: UsbDeviceConnection? = null
    private var serialPort: UsbSerialDevice? = null
    private var signalList = ArrayList<Signal>()
    private var signalListString = mutableListOf<String>()
    private lateinit var listArrayAdapter: ArrayAdapter<String>


    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == ACTION_USB_PERMISSION) {
                val granted = intent.extras!!.getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED)
                if (granted) {
                    connection = usbManager?.openDevice(device)

                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection)

                    if (serialPort != null) {

                        if (serialPort!!.open()) { //Установить параметры последовательного соедниения.
                            serialPort!!.setBaudRate(9600)
                            serialPort!!.setDataBits(UsbSerialInterface.DATA_BITS_8)
                            serialPort!!.setStopBits(UsbSerialInterface.STOP_BITS_1)
                            serialPort!!.setParity(UsbSerialInterface.PARITY_NONE)
                            serialPort!!.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF)
                            serialPort!!.read(mCallback) //

                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN")
                        }

                    } else {
                        Log.d("SERIAL", "PORT IS NULL")
                    }

                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED")
                }

            } else if (intent.action == UsbManager.ACTION_USB_DEVICE_ATTACHED) {
                startCommunication()
            } else if (intent.action == UsbManager.ACTION_USB_DEVICE_DETACHED) {
                stopCommunication()
            }
        }
    }

    private var mCallback: UsbSerialInterface.UsbReadCallback = UsbSerialInterface.UsbReadCallback { arg0 ->
        // Определение метода обратного вызова, который вызывается при приеме данных.
        try {

            if (arg0 != null && arg0.isNotEmpty()) {
                var isAlreadyExists = false

                val signal = Signal(arg0[0].toInt(), String(arg0, charset("UTF-8")), currentVID)

                if (signalListString.contains(signal.toString()))
                    isAlreadyExists = true

                /*if (0 != signalList.size) {
                    val signalIterator = signalList.iterator()

                    while (signalIterator.hasNext())

                        if (signalIterator.next().signalCodeInt == signal.signalCodeInt) {
                            isAlreadyExists = true
                            break
                        }

                }*/

                if (!isAlreadyExists) {
                    signalList.add(signal)
                    signalListString.add(signal.toString())

                    this@CatchSignalActivity.runOnUiThread {
                        listArrayAdapter.notifyDataSetChanged()
                    }
                }

            }

        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }

    //endregion

    //endregion

    //region Standard onMethods

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catch_signal)

        initBroadcastReceiver()
        initUART()
        initArrays()
        initAdapters()
        initUI()
    }

    //region Spinner Select

//    override fun onNothingSelected(parent: AdapterView<*>?) {
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//    }

    //endregion

    //endregion

    //region onButtonClick Methods

    fun onSaveClick(view: View) {
        SettingsRepository.saveSignalsSettings(SignalsSettings(ArrayList(signalList)))
    }

    //endregion

    //region Initializators

    /**
     * Инициализация местного BroadcastReceiver.
     */
    private fun initBroadcastReceiver() {
        val filter = IntentFilter()
        filter.addAction(ACTION_USB_PERMISSION)
        registerReceiver(broadcastReceiver, filter)
    }

    /**
     * Инициализация пользовательского интерфейса.
     */
    private fun initUI() {
//        SettingsRepository.clear()

        sw_start_search.setOnCheckedChangeListener {_, isChecked ->
            if (isChecked) {
                startCommunication()
            } else {
                stopCommunication()
            }
        }

        btn_clear.setOnClickListener {
            signalListString.clear()
            signalList.clear()
            this@CatchSignalActivity.runOnUiThread{
                listArrayAdapter.notifyDataSetChanged()
            }
        }

        sp_vids.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentVID = parent?.getItemAtPosition(position).toString().toInt()
            }

        }

    }

    private fun initUART() {
        usbManager = getSystemService(Context.USB_SERVICE) as UsbManager

    }

    private fun initAdapters() {
        val spinnerArrayAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item,
            getVIDs()
        )
        sp_vids.adapter = spinnerArrayAdapter
        currentVID = sp_vids.selectedItem.toString().toInt()

        listArrayAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            signalListString
        )
        lw_signals.adapter = listArrayAdapter
    }

    private fun initArrays() {
        val signalsSettings = SettingsRepository.getSignalsSettings()
        signalList = signalsSettings.signalsArray

        signalListString = signalsSettings.getStringSignalArray()
    }

    //endregion

    //region UART communication methods

    fun startCommunication() {
        initBroadcastReceiver()
        val usbDevices: HashMap<String, UsbDevice>

        if (usbManager != null) {
            usbDevices = usbManager!!.deviceList
        } else return

        if (usbDevices.isNotEmpty()) {
            var keep = true

            for (entry in usbDevices.entries) {
                device = entry.value
                val deviceVID = device!!.vendorId

                if (deviceVID == currentVID) {
                    val pi = PendingIntent.getBroadcast(this, 0,
                        Intent(ACTION_USB_PERMISSION), 0)
                    usbManager!!.requestPermission(device, pi)
                    keep = false
                } else {
                    connection = null
                    device = null
                }

                if (!keep) {
                    break
                }

            }

        }

    }

    fun stopCommunication() {
        unregisterReceiver(broadcastReceiver)
        serialPort?.close()
    }

    //endregion

    //region Private Methods

    /**
     * Получение списка всех VID.
     */
    private fun getVIDs(): MutableList<String> {
        val vidList = mutableListOf<String>()


        val usbDevices: HashMap<String, UsbDevice>

        if (usbManager != null) {
            usbDevices = usbManager!!.deviceList
        } else return vidList

        if (usbDevices.isNotEmpty()) {

            for (entry in usbDevices.entries) {
                val device = entry.value
                vidList.add(device.vendorId.toString())
            }

        }

        return vidList
    }



    //endregion

}
