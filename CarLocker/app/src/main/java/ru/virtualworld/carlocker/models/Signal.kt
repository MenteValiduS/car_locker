package ru.virtualworld.carlocker.models

data class Signal (
    val signalCodeInt: Int,
    val signalCodeChar: String,
    val vid: Int
) {
    override fun toString(): String {
        return "Char: $signalCodeChar | Int: $signalCodeInt | VID: $vid"
    }
}