package ru.virtualworld.carlocker.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.ui.Base.BaseSettingActivity

class SettingsActivity : BaseSettingActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }
}
