package ru.virtualworld.carlocker.models

data class TimingsSettings (
    val gameTime: Long = 10 * SECOND,
    val awaitingTime: Long = 15 * SECOND,
    val demonstrationTime: Long = 15 * SECOND,
    val afterCoinInjectTime: Long = 5 * SECOND,
    val warningTime: Long = 5 * SECOND,
    val afterEndGameTime: Long = 10 * SECOND

) {
    companion object {
        private const val SECOND = 1000L
        private const val MINUTE = 60 * SECOND
    }

    val timeInSeconds
    get() = gameTime / 1000

    val timeInMinutes
    get() = gameTime / 60000
}