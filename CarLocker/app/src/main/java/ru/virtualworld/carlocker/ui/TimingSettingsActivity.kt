package ru.virtualworld.carlocker.ui

import android.os.Bundle
import android.view.View
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_timing_settings.*
import ru.virtualworld.carlocker.R
import ru.virtualworld.carlocker.models.TimingsSettings
import ru.virtualworld.carlocker.repositories.SettingsRepository
import ru.virtualworld.carlocker.ui.Base.BaseSettingActivity

class TimingSettingsActivity : BaseSettingActivity() {
    companion object {
        private const val LONG_SECOND = 1000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timing_settings)
    }

    fun onAssignTimeClick(view: View) {
        SettingsRepository.saveTimingsSettings(TimingsSettings(
            extractSeconds(et_gaming_time),
            extractSeconds(et_awaiting_time),
            extractSeconds(et_demonstration_time),
            extractSeconds(et_after_coin_inject_time),
            extractSeconds(et_warning_time),
            extractSeconds(et_after_endgame_time)
            ))
    }

    private fun extractSeconds(editText: EditText): Long {
        return editText.toString().toLong() * LONG_SECOND
    }
}
