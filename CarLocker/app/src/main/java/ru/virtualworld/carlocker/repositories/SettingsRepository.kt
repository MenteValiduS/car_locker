package ru.virtualworld.carlocker.repositories

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.virtualworld.carlocker.App
import ru.virtualworld.carlocker.models.SignalsSet
import ru.virtualworld.carlocker.models.SignalsSettings
import ru.virtualworld.carlocker.models.TimingsSettings

object SettingsRepository {

    //region TimingsSettings

    private const val TIME = "TIME"
    private const val PACKAGE_NAME = "PACKAGE_NAME"
    private const val GameTime = "gameTime"
    private const val AwaitingTime = "awaitingTime"
    private const val DemonstrationTime = "demonstrationTime"
    private const val AfterCoinInjectTime = "afterCoinInjectTime"
    private const val WarningTime = "warningTime"
    private const val AfterEndGameTime = "afterEndGameTime"

    //endregion

    //region SignalsSettings

    private const val SIGNALS_SETTINGS = "SIGNALS_SETTINGS"

    //endregion

    //region SignalsSet

    private const val SIGNALS_SET = "SIGNALS_SET"

    //endregion

    private val prefs: SharedPreferences by lazy {
        val ctx = App.applicationContext()
        PreferenceManager.getDefaultSharedPreferences(ctx)
    }

    fun getTimingsSettings(): TimingsSettings = TimingsSettings(
        prefs.getLong(GameTime, 10000L),
        prefs.getLong(AwaitingTime, 10000L),
        prefs.getLong(DemonstrationTime, 10000L),
        prefs.getLong(AfterCoinInjectTime, 10000L),
        prefs.getLong(WarningTime, 10000L),
        prefs.getLong(AfterEndGameTime, 10000L)
    )

    fun saveTimingsSettings(timingsSettings: TimingsSettings) {



        with(timingsSettings) {
            putValue(GameTime to gameTime)
            putValue(AwaitingTime to awaitingTime)
            putValue(DemonstrationTime to demonstrationTime)
            putValue(AfterCoinInjectTime to afterCoinInjectTime)
            putValue(WarningTime to warningTime)
            putValue(AfterEndGameTime to afterEndGameTime)
        }

    }

    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)

    fun getSignalsSettings(): SignalsSettings {
        val gson = Gson()
        return gson.fromJson<SignalsSettings>(prefs.getString(SIGNALS_SETTINGS, "")!!)
            ?: SignalsSettings(ArrayList())
    }

    fun saveSignalsSettings(signalsSettings: SignalsSettings) {
        with(signalsSettings) {
            val gson = Gson()
            val json = gson.toJson(this)
            putValue(SIGNALS_SETTINGS to json)
        }

    }

    fun getSignalsSet(): SignalsSet {
        val gson = Gson()
        return gson.fromJson<SignalsSet>(prefs.getString(SIGNALS_SET, "")!!)
            ?: SignalsSet(emptyMap())
    }

    fun saveSignalsSet(signalsSet: SignalsSet) {
        with(signalsSet) {
            val gson = Gson()
            val json = gson.toJson(this)
            putValue(SIGNALS_SET to json)
        }

    }

    /**
     * Универсальная функция для записи примитивов в репозиторий.
     */
    private fun putValue(pair: Pair<String, Any>) = with(prefs.edit()) {
        val key = pair.first
        val value = pair.second

        when (value) {
            is Int -> putInt(key, value)
            is String -> putString(key, value)
            is Long -> putLong(key, value)
            is Float -> putFloat(key, value)
            else -> error("Only primitives!!")
        }

        apply()
    }

    fun clear() {
        with(prefs.edit()) {
            clear()
            apply()
        }

    }

}